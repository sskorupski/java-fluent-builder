package com.sskorupski.common;

public interface Converter<F,T> {

    T convert(F from);
}
