package com.sskorupski.intellij.plugin.generator.builder;

import com.intellij.ide.util.DefaultPsiElementCellRenderer;
import com.intellij.openapi.ui.DialogWrapper;
import com.intellij.openapi.ui.LabeledComponent;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiField;
import com.intellij.ui.CollectionListModel;
import com.intellij.ui.ToolbarDecorator;
import com.intellij.ui.components.JBList;
import com.sskorupski.intellij.plugin.generator.builder.BuilderDialog;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

import static com.intellij.openapi.ui.LabeledComponent.create;
import static com.intellij.ui.ToolbarDecorator.createDecorator;

public abstract class BuilderDialogImpl extends DialogWrapper implements BuilderDialog {

    private final LabeledComponent<JPanel> labeledComponent;
    private final JList ownerClassFields;
    private final PsiClass ownerClass;
    private final String builderClassSuffix;

    public BuilderDialogImpl(PsiClass ownerClass, String builderClassSuffix) {
        super(ownerClass.getProject());

        this.ownerClass = ownerClass;
        this.builderClassSuffix = builderClassSuffix;
        ownerClassFields = buildOwnerClassFields();
        labeledComponent = buildLabeledComponent();

        init();
    }

    @Override
    protected void init() {
        setTitle(getDialogTitle());
        super.init();
    }

    @NotNull
    private JList buildOwnerClassFields() {
        CollectionListModel<PsiField> fields = new CollectionListModel<PsiField>(getFieldsToAdd());
        JBList fieldsList = new JBList(fields);
        fieldsList.setCellRenderer(new DefaultPsiElementCellRenderer());
        return fieldsList;
    }

    private PsiField[] getFieldsToAdd() {
        PsiClass builderClass = ownerClass.findInnerClassByName(builderClassSuffix, true);
        if (builderClass != null) {
            return additionalFields(builderClass);
        }
        return ownerClass.getFields();
    }

    private PsiField[] additionalFields(PsiClass builderClass) {
        ArrayList<PsiField> additionalFields = new ArrayList<>();
        boolean searchInBaseClass = true;
        for (PsiField psiField : ownerClass.getFields()) {
            if (builderClass.findFieldByName(psiField.getName(), searchInBaseClass) == null) {
                additionalFields.add(psiField);
            }
        }
        return additionalFields.toArray(new PsiField[]{});
    }

    @NotNull
    private LabeledComponent<JPanel> buildLabeledComponent() {
        JPanel panel = buildToolbarDecorator().createPanel();
        return create(panel, getPanelTitle());
    }


    @NotNull
    private ToolbarDecorator buildToolbarDecorator() {
        ToolbarDecorator decorator = createDecorator(ownerClassFields);
        decorator.disableAddAction();
        decorator.disableRemoveAction();
        decorator.disableUpDownActions();
        return decorator;
    }

    @Override
    protected JComponent createCenterPanel() {
        return labeledComponent;
    }

    public List<PsiField> getFields() {
        return ownerClassFields.getSelectedValuesList();
    }
}
