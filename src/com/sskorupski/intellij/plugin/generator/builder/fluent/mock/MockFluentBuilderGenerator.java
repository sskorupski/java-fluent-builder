package com.sskorupski.intellij.plugin.generator.builder.fluent.mock;

import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiField;
import com.intellij.psi.PsiMethod;
import com.sskorupski.intellij.plugin.generator.builder.BuilderGeneratorImpl;
import org.apache.commons.lang.WordUtils;

import static com.intellij.psi.JavaPsiFacade.getElementFactory;

public class MockFluentBuilderGenerator extends BuilderGeneratorImpl {


    public MockFluentBuilderGenerator(PsiClass ownerClass, String builderSuffix) {
        super(ownerClass, builderSuffix);
    }

    protected PsiMethod createBuilderMethod(PsiField field) {
        String builderMethodText =
            "public " + getBuilderClassName() + " " + field.getName() + "(" + field.getType().getPresentableText() + " " + field.getName() + "){\n"
                + "  Mockito.when(this." + getModelFieldName() + ".get" + WordUtils.capitalize(field.getName()) + "()\n"
                + "      .thenReturn(" + field.getName() + ");\n"
                + "  return this;\n"
                + "}";
        return getElementFactory(field.getProject()).createMethodFromText(builderMethodText, null);
    }

}
