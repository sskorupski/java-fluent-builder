package com.sskorupski.intellij.plugin.generator.builder.fluent.mock;

import com.intellij.psi.PsiClass;
import com.sskorupski.intellij.plugin.generator.builder.BuilderDialogImpl;

public class MockFluentBuilderDialog extends BuilderDialogImpl{

    public MockFluentBuilderDialog(PsiClass ownerClass, String builderSuffix) {
        super(ownerClass, builderSuffix);
    }

    @Override
    public String getDialogTitle() {
        return "Select fields you want to generate as mock builder methods";
    }

    @Override
    public String getPanelTitle() {
        return "Available fields";
    }

}
