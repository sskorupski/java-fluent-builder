package com.sskorupski.intellij.plugin.generator.builder.fluent;

import com.intellij.psi.PsiClass;
import com.sskorupski.intellij.plugin.generator.builder.BuilderAction;
import com.sskorupski.intellij.plugin.generator.builder.BuilderDialog;
import com.sskorupski.intellij.plugin.generator.builder.BuilderGenerator;

public class FluentBuilderAction extends BuilderAction {

    private static final String builderSuffix = "Builder";


    @Override
    protected BuilderDialog createBuilderDialog(PsiClass psiClass) {
        return new FluentBuilderDialog(psiClass, builderSuffix);
    }

    @Override
    protected BuilderGenerator createBuilderGenerator(PsiClass psiClass) {
        return new FluentBuilderGenerator(psiClass, builderSuffix);
    }
}
