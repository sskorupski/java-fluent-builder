package com.sskorupski.intellij.plugin.generator.builder.fluent.mock;

import com.intellij.psi.PsiClass;
import com.sskorupski.intellij.plugin.generator.builder.BuilderAction;
import com.sskorupski.intellij.plugin.generator.builder.BuilderDialog;
import com.sskorupski.intellij.plugin.generator.builder.BuilderGenerator;

public class MockFluentBuilderAction extends BuilderAction {

    private static final String builderSuffix = "FluentBuilder";

    @Override
    protected BuilderDialog createBuilderDialog(PsiClass psiClass) {
        return new MockFluentBuilderDialog(psiClass, builderSuffix);
    }

    @Override
    protected BuilderGenerator createBuilderGenerator(PsiClass psiClass) {
        return new MockFluentBuilderGenerator(psiClass, builderSuffix);
    }
}
