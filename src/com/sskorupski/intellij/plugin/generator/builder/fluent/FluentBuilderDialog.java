package com.sskorupski.intellij.plugin.generator.builder.fluent;

import com.intellij.ide.util.DefaultPsiElementCellRenderer;
import com.intellij.openapi.ui.DialogWrapper;
import com.intellij.openapi.ui.LabeledComponent;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiField;
import com.intellij.ui.CollectionListModel;
import com.intellij.ui.ToolbarDecorator;
import com.intellij.ui.components.JBList;
import com.sskorupski.intellij.plugin.generator.builder.BuilderDialog;
import com.sskorupski.intellij.plugin.generator.builder.BuilderDialogImpl;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

import static com.intellij.openapi.ui.LabeledComponent.create;
import static com.intellij.ui.ToolbarDecorator.createDecorator;

public class FluentBuilderDialog extends BuilderDialogImpl{

    public FluentBuilderDialog(PsiClass ownerClass, String builderSuffix) {
        super(ownerClass, builderSuffix);
    }

    @Override
    public String getDialogTitle() {
        return "Select fields you want to generate as builder methods";
    }

    @Override
    public String getPanelTitle() {
        return "Available fields";
    }

}
