package com.sskorupski.intellij.plugin.generator.builder.fluent;

import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiField;
import com.intellij.psi.PsiMethod;
import com.sskorupski.intellij.plugin.generator.builder.BuilderGeneratorImpl;
import org.apache.commons.lang.WordUtils;

import static com.intellij.psi.JavaPsiFacade.getElementFactory;

public class FluentBuilderGenerator extends BuilderGeneratorImpl {


    public FluentBuilderGenerator(PsiClass ownerClass, String builderSuffix) {
        super(ownerClass, builderSuffix);
    }

    protected PsiMethod createBuilderMethod(PsiField field) {
        String builderMethodText =
            "public " + getBuilderClassName() + " " + field.getName() + "(" + field.getType().getPresentableText() + " " + field.getName() + "){\n"
                + "  this." + getModelFieldName() + ".set" + WordUtils.capitalize(field.getName()) + "(" + field.getName() + ");\n"
                + "  return this;\n"
                + "}";
        return getElementFactory(field.getProject()).createMethodFromText(builderMethodText, null);
    }


}
