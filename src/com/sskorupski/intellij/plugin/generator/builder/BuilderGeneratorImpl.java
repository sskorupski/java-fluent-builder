package com.sskorupski.intellij.plugin.generator.builder;

import com.intellij.openapi.command.WriteCommandAction;
import com.intellij.psi.*;
import org.apache.commons.lang.WordUtils;
import org.jetbrains.annotations.NotNull;

import java.util.List;

import static com.intellij.psi.JavaPsiFacade.getElementFactory;

public abstract class BuilderGeneratorImpl implements BuilderGenerator {

    private final PsiClass ownerClass;
    private final PsiElementFactory elementFactory;
    private final String modelFieldName;
    private String builderClassName;

    public BuilderGeneratorImpl(PsiClass ownerClass, String builderSuffix) {
        this.ownerClass = ownerClass;
        builderClassName = buildBuilderClassName(builderSuffix);
        modelFieldName = buildModelFieldName();
        elementFactory = getElementFactory(ownerClass.getProject());
    }

    protected String getBuilderClassName() {
        return builderClassName;
    }

    protected String getModelFieldName() {
        return modelFieldName;
    }

    @NotNull
    private String buildBuilderClassName(String builderSuffix) {
        return ownerClass.getName() + builderSuffix;
    }

    @NotNull
    private String buildModelFieldName() {
        return WordUtils.uncapitalize(ownerClass.getName());
    }

    @Override
    public void generate(List<PsiField> fieldsToGenerate) {
        new WriteCommandAction.Simple(ownerClass.getProject(), ownerClass.getContainingFile()) {
            @Override
            protected void run() throws Throwable {
                createBuilderClass(fieldsToGenerate);
            }
        }.execute();
    }

    private void createBuilderClass(List<PsiField> fieldsToGenerate) {
        PsiClass builderClass = getBuilderClass();

        if (builderClass == null) {
            builderClass = elementFactory.createClass(builderClassName);
            builderClass.add(createModelField());
            builderClass.add(createSimpleConstructor());
            builderClass.add(createParamConstructor());
            builderClass.add(createBuildMethod());
        }

        for (PsiField field : fieldsToGenerate) {
            builderClass.add(createBuilderMethod(field));
        }

        if (getBuilderClass() == null) {
            ownerClass.add(builderClass);
        }
    }

    /**
     * @return Owner class private field
     */
    private PsiElement createModelField() {
        String modelFieldText = "private " + ownerClass.getName() + " " + modelFieldName + ";";
        return elementFactory.createFieldFromText(modelFieldText, null);
    }

    @NotNull
    private PsiMethod createSimpleConstructor() {
        String simpleConstructorText =
            "public " + builderClassName + "(){\n "
                + "this." + modelFieldName + " = new " + ownerClass.getName() + "();\n"
                + "}";
        return createMethod(simpleConstructorText);
    }

    private PsiElement createParamConstructor() {
        String paramConstructor =
            "public " + builderClassName +
                "(" + ownerClass.getName() + " " + modelFieldName + "){\n " +
                "   this." + modelFieldName+ " = " + modelFieldName + ";\n"
                + "}";
        return createMethod(paramConstructor);
    }

    /**
     * @return The build() method that return the model field
     */
    private PsiMethod createBuildMethod() {
        String buildMethodText =
            "public " + ownerClass.getName() + " build(){\n" +
                "   return this." + modelFieldName + ";\n" +
                "}";
        return createMethod(buildMethodText);
    }

    protected abstract PsiMethod createBuilderMethod(PsiField field);

    private PsiClass getBuilderClass() {
        return ownerClass.findInnerClassByName(builderClassName, true);
    }

    private PsiMethod createMethod(String methodText) {
        return elementFactory.createMethodFromText(methodText, null);
    }
}
