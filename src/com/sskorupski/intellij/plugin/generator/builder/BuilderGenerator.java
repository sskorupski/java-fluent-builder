package com.sskorupski.intellij.plugin.generator.builder;

import com.intellij.psi.PsiField;

import java.util.List;

public interface BuilderGenerator {

    void generate(List<PsiField> fieldsToGenerate);
}
