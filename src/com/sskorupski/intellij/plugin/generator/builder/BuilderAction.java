package com.sskorupski.intellij.plugin.generator.builder;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.editor.Editor;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiField;
import com.intellij.psi.PsiFile;
import com.sskorupski.intellij.plugin.generator.builder.fluent.FluentBuilderGenerator;

import java.util.List;

import static com.intellij.openapi.actionSystem.CommonDataKeys.EDITOR;
import static com.intellij.openapi.actionSystem.CommonDataKeys.PSI_FILE;
import static com.intellij.psi.util.PsiTreeUtil.getParentOfType;

public abstract class BuilderAction extends AnAction {

    @Override
    public void actionPerformed(AnActionEvent e) {
        PsiClass psiClass = getPsiClass(e);
        BuilderDialog builderDialog = createBuilderDialog(psiClass);
        builderDialog.show();
        if (builderDialog.isOK()) {
            List<PsiField> fields = builderDialog.getFields();
            if(fields.size() > 0){
                createBuilderGenerator(psiClass).generate(fields);
            }
        }
    }

    protected abstract BuilderDialog createBuilderDialog(PsiClass psiClass);
    protected abstract BuilderGenerator createBuilderGenerator(PsiClass psiClass);

    @Override
    public void update(AnActionEvent e) {
        PsiClass psiClass = getPsiClass(e);
        e.getPresentation().setEnabled(psiClass != null);
    }

    private PsiClass getPsiClass(AnActionEvent e) {
        PsiFile psiFile = e.getData(PSI_FILE);
        Editor editor = e.getData(EDITOR);
        if (psiFile == null || editor == null) {
            return null;
        }
        int offset = editor.getCaretModel().getOffset();
        PsiElement elementAt = psiFile.findElementAt(offset);
        return getParentOfType(elementAt, PsiClass.class);
    }
}
